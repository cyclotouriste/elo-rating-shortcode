<?php
/*
Plugin Name: Elo Rating Shortcode
Plugin URI: https://wordpress.org/plugins/elo-rating-shortcode/
Description: Add a Calculator for Elo Rating to your website with a simple shortcode.
Version: 1.0.4
Author: Marcel Pol
Author URI: https://timelord.nl
Text Domain: elo-rating-shortcode
Domain Path: /lang/


Copyright 2015 - 2024  Marcel Pol (email: marcel@zenoweb.nl)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


// Plugin Version
define('ELO_RATING_SHORTCODE_VER', '1.0.4');

define('ELO_RATING_SHORTCODE_FOLDER', plugin_basename( __DIR__ ));
define('ELO_RATING_SHORTCODE_DIR', WP_PLUGIN_DIR . '/' . ELO_RATING_SHORTCODE_FOLDER);


/*
 * Really only load this when our shortcode runs.
 */
function elo_rating_shortcode_init() {
	wp_enqueue_script('jquery');
	wp_enqueue_script('elo-rating-shortcode-js', plugins_url('elo-rating-shortcode.js', __FILE__), array( 'jquery' ), ELO_RATING_SHORTCODE_VER, true);
}


/*
 * Load Language files for frontend and backend.
 */
function elo_rating_shortcode_load_lang() {
	load_plugin_textdomain( 'elo-rating-shortcode', false, __DIR__ . '/lang' );
}
add_action( 'init', 'elo_rating_shortcode_load_lang' );


/* Include the shortcodes. */
require_once ELO_RATING_SHORTCODE_DIR . '/shortcodes.php';

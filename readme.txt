=== Elo Rating Shortcode ===
Contributors: mpol
Tags: elo, elo rating, competition, chess, board games, games, shortcode, calculations, calculator
Requires at least: 4.1
Tested up to: 6.7
Stable tag: 1.0.4
License: GPLv2
Requires PHP: 7.0

Add a Calculator for Elo Rating to your website with a simple shortcode.

== Description ==

Add a Calculator for Elo Rating to your website with a simple shortcode.

Features:

* Simple Shortcode for simple Elo calculations.
* Extended Shortcode for extended Elo calculations.
* Almost no configuration needed.
* Simple and Lightweight.
* Uses JavaScript for the calculations, so no page reload.

= Demo =

Check out the demo at [my local chessclub Pegasus](https://svpegasus.nl/algemeen/knsb-rating/elo-rating-berekenen/)

= Compatibility =

This plugin is compatible with [ClassicPress](https://www.classicpress.net).

= Contributions =

This plugin is also available in [Codeberg](https://codeberg.org/cyclotouriste/elo-rating-shortcode).

== Installation ==

1. Upload the directory `elo-rating-shortcode` to the `/wp-content/plugins/` directory or install the plugin directly with the 'Install' function in the 'Plugins' menu in WordPress.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Add a shortcode '[elo_rating_shortcode]' to your page.
4. Enjoy!


For extended Elo calculations use the '[elo_rating_shortcode_extended]' shortcode.


== Screenshots ==

1. An example of the main shortcode.


== Frequently Asked Questions ==

= How can I use a different K factor? =

The default K factor = 25.

You can add a parameter to the shortcode to have a custom value, like:

	[elo-rating_shortcode k_factor="20"]

== Changelog ==

= 1.0.4 =
* 2024-10-13
* Fix security issue (thanks theviper17 at patchstack).
* K factor should always be an integer.

= 1.0.3 =
* 2024-10-02
* Loading plugin translations should be delayed until init action.
* Use __DIR__ instead of dirname(__FILE__).

= 1.0.2 =
* 2021-06-01
* Use 'esc_html()' for translations.
* Some updates from wpcs.

= 1.0.1 =
* 2021-02-23
* Fix deprecated jQuery calls with WP 5.6 and jQuery 3.5.

= 1.0.0 =
* 2016-05-21
* First stable version
* Publish the plugin



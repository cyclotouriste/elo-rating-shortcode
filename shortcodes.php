<?php


/*
 * Shortcode for simple Elo Calculator.
 */
function er_elo_rating_shortcode( $atts ) {

	elo_rating_shortcode_init();

	$shortcode_atts = shortcode_atts( array( 'k_factor' => 25 ), $atts );

	$shortcode = '
	<h2>' . esc_html__('Elo Rating Change Calculator', 'elo-rating-shortcode') . '</h2>

	' . esc_html__("This section will calculate the change in a player's Elo rating after playing a	single game against another player. The value K is the maximum change in rating.", 'elo-rating-shortcode') . '

	<form action="#" name="elo_rating_change" id="elo_rating_change">
		<table>
			<tr>
				<td>
					' . esc_html__('Player Elo:', 'elo-rating-shortcode') . '
				</td>
				<td>
					<input name="elo1" class="elo1" value="2000" size="8"/>
				</td>
			</tr>
			<tr>
				<td>
					' . esc_html__('Against Elo:', 'elo-rating-shortcode') . '
				</td>
				<td>
					<input name="elo2" class="elo2" value="2000" size="8"/>
				</td>
			</tr>
			<tr>
				<td>
					' . esc_html__('K factor:', 'elo-rating-shortcode') . '
				</td>
				<td>
					<input name="K" class="K" value="' . (int) $shortcode_atts['k_factor'] . '" size="4"/>
				</td>
			</tr>
		</table>

		<input type="button" value="' . esc_html__('Calculate Change', 'elo-rating-shortcode') . '" class="button"/>

		<table>
			<tr>
				<td width="33%">' . esc_html__('Win', 'elo-rating-shortcode') . '</td>
				<td width="33%">' . esc_html__('Draw', 'elo-rating-shortcode') . '</td>
				<td width="33%">' . esc_html__('Loss', 'elo-rating-shortcode') . '</td>
			</tr>
			<tr>
				<td><input name="win"  class="win"  value="0" size="8" /></td>
				<td><input name="draw" class="draw" value="0" size="8" /></td>
				<td><input name="loss" class="loss" value="0" size="8" /></td>
			</tr>
		</table>

		' . esc_html__('Expected Percentage:', 'elo-rating-shortcode') . ' <input name="percent" class="percent" value="0" size="8" />

	</form>
	';

	return $shortcode;

}
add_shortcode( 'elo_rating_shortcode', 'er_elo_rating_shortcode' );


/*
 * Shortcode for extended Elo Calculator.
 */
function er_elo_rating_shortcode_extended( $atts ) {

	elo_rating_shortcode_init();

	$shortcode = '
	<h2>' . esc_html__('Elo Rating Difference Calculator', 'elo-rating-shortcode') . '</h2>

	' . esc_html__('This section will calculate the difference in Elo rating between two players from match results or winning percentage.', 'elo-rating-shortcode') . '

	<form name="elo_rating_score" id="elo_rating_score" action="#">
		<table>
			<tr>
				<td>' . esc_html__('Wins', 'elo-rating-shortcode') . '</td>
				<td>' . esc_html__('Losses', 'elo-rating-shortcode') . '</td>
				<td>' . esc_html__('Draws', 'elo-rating-shortcode') . '</td>
			</tr>
			<tr>
				<td><input name="wins"   class="wins"   value="0" size="5"/></td>
				<td><input name="losses" class="losses" value="0" size="5"/></td>
				<td><input name="draws"  class="draws"  value="0" size="5"/></td>
			</tr>
		</table>

		<input type="button" value="' . esc_html__('Calculate Elo', 'elo-rating-shortcode') . '" class="button"/>
	</form>


	<form name="elo_rating_points" id="elo_rating_points" action="#">
		<table>
			<tr>
				<td>' . esc_html__('Points', 'elo-rating-shortcode') . '</td>
				<td></td>
				<td>' . esc_html__('Games', 'elo-rating-shortcode') . '</td>
			</tr>
			<tr>
				<td><input name="points" class="points" value="0" size="5"/></td>
				<td>/</td>
				<td><input name="totalgames" class="totalgames" value="0" size="5"/></td>
			</tr>
		</table>

		<input type="button" value="' . esc_html__('Calculate Elo', 'elo-rating-shortcode') . '" class="button"/>
	</form>


	<form name="elo_rating_percent" id="elo_rating_percent" action="#">
		<table>
			<tr>
				<td>' . esc_html__('Winning percentage', 'elo-rating-shortcode') . '</td>
			</tr>
			<tr>
				<td><input name="winning" class="winning" value="50" size="5"/></td>
			</tr>
		</table>

		<input type="button" value="' . esc_html__('Calculate Elo', 'elo-rating-shortcode') . '" class="button"/>

		<table>
			<tr>
				<td>' . esc_html__('Elo Difference:', 'elo-rating-shortcode') . ' <input name="difference" class="difference" value="+0" size="8"/></td>
			</tr>
		</table>

	</form>
	';

	return $shortcode;

}
add_shortcode( 'elo_rating_shortcode_extended', 'er_elo_rating_shortcode_extended' );
